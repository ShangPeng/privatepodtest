//
//  NSDictionary+Extension.h
//  TRSMobileV2
//
//  Created by  TRS on 16/3/16.
//  Copyright © 2016年  TRS. All rights reserved.
//

#import <Foundation/Foundation.h>

#define valueForDictionaryFile(filename) [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:filename ofType:@"plist"] ]

@interface NSDictionary (NSDictionary_Extension)

/**
 * 字典转化为JSON字符串
 * @param dictionary 字典
 */

+ (NSString *)dictionaryToJSONString:(NSDictionary *)dictionary;


+ (NSString *)dictionaryToURLString:(NSDictionary *)dictionary ;


/**
 * JSON字符串转化为字典
 * @param jsonString json
 */
+ (NSDictionary *)dictionaryFromJSONString:(NSString *)jsonString;


/**
 * 根据虚拟的键字段获取字典的值，用于由键不固定而需要获取键值的情况
 * @param key key
 */
- (NSString *)objectForVitualKey:(NSString *)key;

/**
 * 安全获取字典的键值，避免网络数据的键值为空而引起的程序crash
 * @param key key
 */
- (id)safeObjectForKey:(NSString *)key;

/**
 * 安全设置字典的键值，避免对象键值为空而引起的程序crash
 * @param object object
 * @param key    key
 */
- (void)setSafeObject:(id)object forKey:(NSString *)key;


//将字典安全化，把所有可能是空的值变成字符串的@“”
-(NSDictionary *)safeDicNullIsStr;




+ (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonStr;
- (NSString*)jsonString;
@end
