//
//  UIView+Extension.h
//  TRSMobileV2
//
//  Created by  TRS on 16/3/25.
//  Copyright © 2016年  TRS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

#define GETSIZEMAX(w,h) w>h?w:h

#define GETSIZEMIN(w,h) w>h?h:w
@interface UIView (UIView_CALayer)

/**
 * 设置圆角
 * @param radius : 半径
 */
- (void)setCornerWithRadius:(CGFloat)radius;


/**
 * 设置边框
 * @param color : 颜色
 * @param borderWidth : 宽度
 */
- (void)setBorderWithColor:(UIColor *)color borderWidth:(CGFloat)borderWidth;

/**
 * 设置阴影
 * @param color         : 颜色
 * @param shadowOpacity : 宽度
 */
- (void)setShadowWithColor:(UIColor *)color
             shadowOpacity:(CGFloat)shadowOpacity
              shadowOffset:(CGSize)shadowOffset;


/**
 * 设置渐变色
 * @param gradientColors : 颜色
 * @param gradientRect   : 宽度
 */
- (void)setGradientWithColors:(NSArray *)gradientColors
                 gradientRect:(CGRect )gradientRect;

@end


@interface UIView  (UIView_CGRect)


+ (NSString *)identifier;

- (void)removeAllSubviews;

- (UIViewController *)viewController;


@end

@interface UIView (UIView_UIGestureRecognizer)

/**
 * 点击手势
 */
- (void)addTapGesture:(id)target selector:(SEL)selector;


/**
 * 拖动手势
 */
- (void)addPanGesture:(id)target selector:(SEL)selector;

/**
 * 长按手势
 */
- (void)addLongPressGesture:(id)target selector:(SEL)selector;

@end


@interface UIView  (UIView_UIResponder)

/**
 * 查找响应链FirstResponder
 */
- (UIView *)findFirstResponder;

/**
 * 查找当前view的是否有scrollView
 */
- (UIScrollView *)findScrollView;

@end

@interface UIView  (UIView_Nib)

/**
 * 从xib加载视图
 */
+ (UIView *)viewSharedInstance;
@end
@interface UIView (Extension)
@property (nonatomic, assign) CGFloat x;
@property (nonatomic, assign) CGFloat y;
@property (nonatomic, assign) CGFloat centerX;
@property (nonatomic, assign) CGFloat centerY;
@property (nonatomic, assign) CGFloat width;
@property (nonatomic, assign) CGFloat height;
@property (nonatomic, assign) CGSize size;
@property (nonatomic, assign) CGPoint origin;

@property (nonatomic, assign) CGFloat left;
@property (nonatomic, assign) CGFloat right;
@property (nonatomic, assign) CGFloat top;
@property (nonatomic, assign) CGFloat bottom;


@property (nonatomic, assign) CGFloat size_max;
@property (nonatomic, assign) CGFloat size_min;

@property (nonatomic, assign) CGFloat x_w;
@property (nonatomic, assign) CGFloat y_h;


/*--------------------点------------------------*/
/**
 *  左下角
 */
@property (readonly) CGPoint bottomLeft;

/**
 *  右下角
 */
@property (readonly) CGPoint bottomRight;

/**
 *  右上角
 */
@property (readonly) CGPoint topRight;

/**
 *  左上角
 */
@property (readonly) CGPoint topLeft;

//CAShapeLayer *_lineLayer;
//CGFloat _line_height;

/**
 *  绘制cell分割线
 *
 *  @param rect      cell_rect
 *  @param lineWidth 线宽
 *  @param lineColor 线色
 */
- (void)drawCellSeparatorLine:(CGRect)rect lineWidth:(CGFloat)lineWidth lineColor:(UIColor *)lineColor;

@property (nonatomic,strong)CAShapeLayer *lineLayer;


//获取当前的X
- (CGFloat)current_x;

//获取当前的Y
- (CGFloat)current_y;

//获取当前的H
- (CGFloat)current_h;

//获取当前的W
- (CGFloat)current_w;

//获取当前x+w位置
- (CGFloat)current_x_w;

//获取当前y+h位置
- (CGFloat)current_y_h;




@end

