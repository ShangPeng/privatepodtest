//
//  UIView+Extension.m
//  TRSMobileV2
//
//  Created by  TRS on 16/3/25.
//  Copyright © 2016年  TRS. All rights reserved.
//

#import "UIView+Extension.h"

@interface UIView ()
{
//    CAShapeLayer *_lineLayer;
//    CGFloat _line_height;
}
@end
#pragma mark -
@implementation UIView (UIView_CALayer)


/**
 * 设置圆角
 * @param radius : 半径
 */
- (void)setCornerWithRadius:(CGFloat)radius
{
    
    self.layer.masksToBounds = YES;
    self.layer.cornerRadius  = radius;
}


/**
 * 设置边框
 * @param color : 颜色
 * @param borderWidth : 宽度
 */
- (void)setBorderWithColor:(UIColor *)color
               borderWidth:(CGFloat)borderWidth
{
    
    self.layer.borderColor = color.CGColor;
    self.layer.borderWidth = borderWidth;
}

/**
 * 设置阴影
 * @param color : 颜色
 * @param shadowOpacity : 宽度
 */
- (void)setShadowWithColor:(UIColor *)color
             shadowOpacity:(CGFloat)shadowOpacity
              shadowOffset:(CGSize)shadowOffset
{
    
    self.layer.shadowColor = color.CGColor;
    self.layer.shadowOpacity = shadowOpacity;
    self.layer.shadowOffset  = shadowOffset;
}


/**
 * 设置渐变色
 * @param gradientColors : 颜色
 * @param gradientRect   : 宽度
 */
- (void)setGradientWithColors:(NSArray *)gradientColors
                 gradientRect:(CGRect )gradientRect
{
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = gradientRect;
    gradient.colors = gradientColors;
    [self.layer insertSublayer:gradient atIndex:0];
}

@end


#pragma mark -
@implementation UIView  (UIView_CGRect)

+ (NSString *)identifier
{
    return [NSString stringWithFormat:@"%@", self];
}

- (void)removeAllSubviews {
    while (self.subviews.count) {
        UIView *childView = self.subviews.lastObject;
        [childView removeFromSuperview];
    }
}

/** 获取当前视图所在控制器 */
- (UIViewController *)viewController {
    for (UIView* next = [self superview]; next; next = next.superview) {
        UIResponder *nextResponder = [next nextResponder];
        if ([nextResponder isKindOfClass:[UIViewController class]]) {
            return (UIViewController *)nextResponder;
        }
    }
    return nil;
}


@end


#pragma mark -
@implementation UIView (UIView_UIGestureRecognizer)

/**
 * 点击手势
 */
- (void)addTapGesture:(id)target
             selector:(SEL)selector
{
    UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:target action:selector];
    [self addGestureRecognizer:recognizer];
}

/**
 * 拖动手势
 */
- (void)addPanGesture:(id)target
             selector:(SEL)selector
{
    UIPanGestureRecognizer *recognizer = [[UIPanGestureRecognizer alloc] initWithTarget:target action:selector];
    [self addGestureRecognizer:recognizer];
}

/**
 * 长按手势
 */
- (void)addLongPressGesture:(id)target
                   selector:(SEL)selector
{
    
    UILongPressGestureRecognizer *recognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:target action:selector];
    recognizer.minimumPressDuration = 0.3;
    [self addGestureRecognizer:recognizer];
}

@end

#pragma mark -
@implementation UIView  (UIView_UIResponder)

/**
 * 查找响应链FirstResponder
 */
- (UIView *)findFirstResponder {
    
    if (self.isFirstResponder) return self;
    for (UIView *subView in self.subviews) {
        UIView *firstResponder = [subView findFirstResponder];
        if (firstResponder != nil) return firstResponder;
    }
    return nil;
}


/**
 * 查找当前view的是否有scrollView
 */
- (UIScrollView *)findScrollView {
    
    if ([self isKindOfClass:[UIScrollView class]]) return (UIScrollView *)self;
    for (UIView *subView in [[self.subviews reverseObjectEnumerator] allObjects]) {
        UIScrollView *scrollView = [subView findScrollView];
        if (scrollView != nil) return scrollView;
    }
    return nil;
}

@end

@implementation UIView (UIView_Nib)

/**
 从xib加载
 */
+ (UIView *)viewSharedInstance;
{
    return [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class])
                                          owner:self
                                        options:nil] lastObject];
}


@end



@implementation UIView (Extension)

- (CGFloat)left;
{
    return CGRectGetMinX([self frame]);
}
- (void)setLeft:(CGFloat)x;
{
    CGRect frame = [self frame];
    frame.origin.x = x;
    [self setFrame:frame];
}
- (CGFloat)top;
{
    return CGRectGetMinY([self frame]);
}
- (void)setTop:(CGFloat)y;
{
    CGRect frame = [self frame];
    frame.origin.y = y;
    [self setFrame:frame];
}

- (CGFloat)right;
{
    return CGRectGetMaxX([self frame]);
}

- (void)setRight:(CGFloat)right;
{
    CGRect frame = [self frame];
    frame.origin.x = right - frame.size.width;
    
    [self setFrame:frame];
}

- (CGFloat)bottom;
{
    return CGRectGetMaxY([self frame]);
}

- (void)setBottom:(CGFloat)bottom;
{
    CGRect frame = [self frame];
    frame.origin.y = bottom - frame.size.height;
    
    [self setFrame:frame];
}

- (void)setX:(CGFloat)x
{
    CGRect frame = self.frame;
    frame.origin.x = x;
    self.frame = frame;
}

- (void)setY:(CGFloat)y
{
    CGRect frame = self.frame;
    frame.origin.y = y;
    self.frame = frame;
}

- (CGFloat)x
{
    return self.frame.origin.x;
}

- (CGFloat)y
{
    return self.frame.origin.y;
}

- (void)setCenterX:(CGFloat)centerX
{
    CGPoint center = self.center;
    center.x = centerX;
    self.center = center;
}

- (CGFloat)centerX
{
    return self.center.x;
}

- (void)setCenterY:(CGFloat)centerY
{
    CGPoint center = self.center;
    center.y = centerY;
    self.center = center;
}

- (CGFloat)centerY
{
    return self.center.y;
}

- (void)setWidth:(CGFloat)width
{
    CGRect frame = self.frame;
    frame.size.width = width;
    self.frame = frame;
}

- (void)setHeight:(CGFloat)height
{
    CGRect frame = self.frame;
    frame.size.height = height;
    self.frame = frame;
}

- (CGFloat)height
{
    return self.bounds.size.height;
}

- (CGFloat)width
{
    return self.bounds.size.width;
}

- (void)setSize:(CGSize)size
{
    CGRect frame = self.frame;
    frame.size = size;
    self.frame = frame;
}

- (CGSize)size
{
    return self.frame.size;
}

- (void)setOrigin:(CGPoint)origin
{
    CGRect frame = self.frame;
    frame.origin = origin;
    self.frame = frame;
}

- (CGPoint)origin
{
    return self.frame.origin;
}


//@property (nonatomic, assign) CGFloat size_max;
//@property (nonatomic, assign) CGFloat size_min;
//
//@property (nonatomic, assign) CGFloat x_w;
//@property (nonatomic, assign) CGFloat y_h;
- (CGFloat)size_max{
    return GETSIZEMAX(self.width, self.height);
}

- (CGFloat)size_min{
    return GETSIZEMIN(self.width, self.height);
}

- (CGFloat)x_w{
    return self.x+self.width;
}

- (CGFloat)y_h{
    return self.y+self.height;
}

/*--------------------华丽分割线------------------------*/
- (CGPoint)bottomLeft {
    CGFloat bottomX = self.frame.origin.x;
    CGFloat bottomY = self.frame.origin.y + self.frame.size.height;
    return CGPointMake(bottomX, bottomY);
}

- (CGPoint)bottomRight {
    CGFloat bottomX = self.frame.origin.x + self.frame.size.width;
    CGFloat bottomY = self.frame.origin.y + self.frame.size.height;
    return CGPointMake(bottomX, bottomY);
}

- (CGPoint)topLeft {
    CGFloat topX = self.frame.origin.x;
    CGFloat topY = self.frame.origin.y;
    return CGPointMake(topX, topY);
}

- (CGPoint)topRight {
    CGFloat topX = self.frame.origin.x + self.frame.size.width;
    CGFloat topY = self.frame.origin.y;
    return CGPointMake(topX, topY);
}

- (void)drawCellSeparatorLine:(CGRect)rect lineWidth:(CGFloat)lineWidth lineColor:(UIColor *)lineColor {
    // 开启上下文
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // 上分割线，
    //    CGContextSetStrokeColorWithColor(context,lineColor.CGColor);
    //    CGContextStrokeRect(context, CGRectMake(0, -0.5, rect.size.width, lineWidth));
    
    // 下分割线
    CGContextSetStrokeColorWithColor(context, lineColor.CGColor);
    CGContextStrokeRect(context, CGRectMake(0, rect.size.height-0.5, rect.size.width, lineWidth));
}



- (CGFloat)current_x{
    return self.frame.origin.x;
}


- (CGFloat)current_y{
    return self.frame.origin.y;
}

- (CGFloat)current_h{
    return self.frame.size.height;
}

- (CGFloat)current_w{
    return self.frame.size.width;
}

- (CGFloat)current_x_w{
    return self.frame.origin.x+self.frame.size.width;
}

- (CGFloat)current_y_h{
    return self.frame.origin.y+self.frame.size.height;
}




//- (void)setParams:(NSDictionary *)paramsDictionary
//{
//    objc_setAssociatedObject(self, &kAssociatedParamsObjectKey, paramsDictionary, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
//}
//
//- (NSDictionary *)params
//{
//    return objc_getAssociatedObject(self, &kAssociatedParamsObjectKey);
//}



@end

