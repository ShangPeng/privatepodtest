//
//  NSDictionary+Extension.m
//  TRSMobileV2
//
//  Created by  TRS on 16/3/16.
//  Copyright © 2016年  TRS. All rights reserved.
//

#import "NSDictionary+Extension.h"

@implementation NSDictionary (NSDictionary_Extension)

/**
 * 字典转化为JSON字符串
 * @param dictionary
 */

+ (NSString *)dictionaryToJSONString:(NSDictionary *)dictionary {

    NSString *jsonString = nil;
    NSError *error = nil;
    NSData *data = [NSJSONSerialization dataWithJSONObject:dictionary
                                                   options:NSJSONWritingPrettyPrinted
                                                     error:&error];
    if(error) {
//        LMLogInfo(@"字典转化为JSON字符串错误: %@", error.localizedDescription);
    }
    else {
        jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    }
    
    return jsonString;
}

+ (NSString *)dictionaryToURLString:(NSDictionary *)dictionary {
    
    NSMutableString *post = [NSMutableString stringWithCapacity:0];
    NSArray  *keys = [dictionary allKeys];
    
    int idx = 0;
    for (id item in keys) {
        if(idx < keys.count - 1)
            [post appendFormat:@"%@=%@&",item, dictionary[item] ];
        else
            [post appendFormat:@"%@=%@",item, dictionary[item] ];
        idx++;
    }
    
    return post;
}

/**
 * JSON字符串转化为字典
 * @param jsonString
 */
+ (NSDictionary *)dictionaryFromJSONString:(NSString *)jsonString {

    NSDictionary *dictionary = nil;
    NSError *error = nil;
    if (![jsonString isKindOfClass:[NSString class]]) {
        DDLogInfo(@"---不是字符串----");
    }
    if(jsonString && ![jsonString isEqualToString:@""]) {
        
        // 增加在,} ,]符号之前多了一个逗号的非标准json字符过滤处理
        jsonString = [jsonString stringByReplacingOccurrencesOfString:@",}" withString:@"}"];
        jsonString = [jsonString stringByReplacingOccurrencesOfString:@",]" withString:@"]"];
        
        // 增加回车换行制表符过滤处理
        jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\r" withString:@""];
        jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\t" withString:@""];
        
        NSData *data = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
        if(data && data.length) {
            dictionary = [NSJSONSerialization JSONObjectWithData:data
                                                         options:NSJSONReadingMutableContainers
                                                           error:&error];
            if(error) {
//                LMLogInfo(@"JSON字符串转化为字典错误: %@", error.localizedDescription);
            }
        }
    }
    
    return dictionary;
}


/**
 * 根据虚拟的键字段获取字典的值，用于由键不固定而需要获取键值的情况
 * @param key
 */
- (NSString *)objectForVitualKey:(NSString *)key {

    id result = nil;
    NSArray *array = valueForDictionaryFile(@"keyFliter")[key];
    for(NSString *str in array) {
        result = [self safeObjectForKey:str];
        if(result) {break;}
    }
    return result;
}

/**
 * 安全获取字典的键值，避免网络数据的键值为空而引起的程序crash
 * @param key
 */
- (id)safeObjectForKey:(NSString *)key {

    id object = [self objectForKey:key];
    if([object isKindOfClass:[NSNull class] ]) {
        object = nil;
    }
    
    return object;
}

/**
 * 安全设置字典的键值，避免对象键值为空而引起的程序crash
 * @param object
 * @param key
 */
- (void)setSafeObject:(id)object forKey:(NSString *)key {

    if(object) {
        [self setValue:object forKey:key];
    }
}

//将字典安全化，把所有可能是空的值变成字符串的@“”
-(NSDictionary *)safeDicNullIsStr{
    NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
    for (id key in self) {
        id object = [self objectForKey:key];
        if([object isKindOfClass:[NSNull class]] || object == nil) {
            object = @"";
        }
        [dic setSafeObject:object forKey:key];
    }
    NSDictionary *returnDic = [[NSDictionary alloc]initWithDictionary:dic copyItems:YES];
    return returnDic;
}



+ (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonStr
{
    if (jsonStr == nil) {
        return nil;
    }
    NSData* jsonData = [jsonStr dataUsingEncoding:NSUTF8StringEncoding];
    NSError *err;
    NSMutableDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&err];
    if (err) {
        DDLogInfo(@"json serialize failue");
        return nil;
    }
    return dic;
}

- (NSString*)jsonString
{
    NSData* infoJsonData = [NSJSONSerialization dataWithJSONObject:self options:NSJSONWritingPrettyPrinted error:nil];
    NSString* json = [[NSString alloc] initWithData:infoJsonData encoding:NSUTF8StringEncoding];
    return json;
}

@end
