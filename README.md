# PrivatePodTest

[![CI Status](https://img.shields.io/travis/shangpeng/PrivatePodTest.svg?style=flat)](https://travis-ci.org/shangpeng/PrivatePodTest)
[![Version](https://img.shields.io/cocoapods/v/PrivatePodTest.svg?style=flat)](https://cocoapods.org/pods/PrivatePodTest)
[![License](https://img.shields.io/cocoapods/l/PrivatePodTest.svg?style=flat)](https://cocoapods.org/pods/PrivatePodTest)
[![Platform](https://img.shields.io/cocoapods/p/PrivatePodTest.svg?style=flat)](https://cocoapods.org/pods/PrivatePodTest)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

PrivatePodTest is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'PrivatePodTest'
```

## Author

shangpeng, shangpeng@hoteamsoft.com

## License

PrivatePodTest is available under the MIT license. See the LICENSE file for more info.
