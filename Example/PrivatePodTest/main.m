//
//  main.m
//  PrivatePodTest
//
//  Created by shangpeng on 04/05/2022.
//  Copyright (c) 2022 shangpeng. All rights reserved.
//

@import UIKit;
#import "SVAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SVAppDelegate class]));
    }
}
