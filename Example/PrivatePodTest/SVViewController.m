//
//  SVViewController.m
//  PrivatePodTest
//
//  Created by shangpeng on 04/05/2022.
//  Copyright (c) 2022 shangpeng. All rights reserved.
//

#import "SVViewController.h"
#import "UIView+Extension.h"

@interface SVViewController ()

@end

@implementation SVViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self.view setCornerWithRadius:100];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
